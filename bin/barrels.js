#!/usr/bin/env node

'use strict';

let fs = require('fs');
let path = require('path');
let fsExistsSync = fs.existsSync ? fs.existsSync : path.existsSync;
let commander = require('commander');


commander.version('0.0.1')
  .usage('[options]')
  .option('--file <file>', 'path to file system-config.ts')
  .option('--show <show>', 'show list barrels')
  .action(() => {
  })
  .parse(process.argv);

let file = commander['file'];
if (!commander['file']) {
  file = `${process.cwd()}/src/system-config.ts`
}

try {
  fs.stat(file, (err, stats) => {
    if (!stats || !stats.isFile()) {
      console.error(`File system-config.ts not found on path ${file}`);
      return 0;
    } else {
      let fileData = fs.readFileSync(file, 'utf8');
      let startSeparate = '// StartOwnBarrels';
      let endSeparate = '// EndOwnBarrels';
      let startPosition = fileData.indexOf(startSeparate);
      let endPosition = fileData.indexOf(endSeparate);
      let lines = barrelList();

      let startData = fileData.substr(0, startPosition);
      let endData = fileData.substr(endPosition + endSeparate.length, fileData.length);

      let newData = '';
      newData += `${startData}${startSeparate}\n// ${Date.now()}\nconst ownBarrels = [`;
      lines.forEach(line => {
        newData += `'${line}',\n`;
      });
      newData += `];\n${endSeparate}${endData}`;

      fs.writeFileSync(file, newData);
    }
  });

} catch (er) {
  return 0;
}

function getA2SourcesDir() {
  // todo  брать  из конфига angular-cli  если он есть
  return path.join(getProjectDir(), 'src');
}

function barrelList(dir = getA2SourcesDir()) {
  return getFilesRecursive(dir)
    .filter(file => file.indexOf('index.ts') > -1)
    .map(d => d.replace(dir + '/', ''))
    .map(d => d.replace('/index.ts', ''));
}

function getFilesRecursive(folder, fileTree = []) {
  var fileContents = fs.readdirSync(folder),
    stats;

  fileContents.forEach(function (fileName) {
    stats = fs.lstatSync(folder + '/' + fileName);

    fileTree.push(folder + '/' + fileName);

    if (stats.isDirectory()) {
      getFilesRecursive(folder + '/' + fileName, fileTree);
    }
  });

  return fileTree;
}
function getProjectDir (dir = process.cwd(), iteratorSync = 'package.json', options = {maxdepth: 10}) {
  if (typeof iteratorSync === 'string') {
    var file = iteratorSync;
    iteratorSync = function (dir) {
      return fsExistsSync(path.join(dir, file));
    };
  }
  options = options || {};
  var initialDir = dir;
  var currentDepth = 0;
  while (dir !== path.join(dir, '..')) {
    if (typeof options.maxdepth === 'number' && options.maxdepth >= 0 && currentDepth > options.maxdepth) {
      break
    }
    currentDepth++;
    if (dir.indexOf('../../') !== -1) throw new Error(initialDir + ' is not correct.');
    if (iteratorSync(dir)) return dir;
    dir = path.join(dir, '..');
  }
  throw new Error('not found');
}
