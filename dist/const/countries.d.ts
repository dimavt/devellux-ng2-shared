export declare const countries: {
    "AD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AQ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AX": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "AZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BB": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BJ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BQ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BV": {
        "alpha2": string;
        "countryCallingCodes": any[];
        "name": string;
    };
    "BW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "BZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CV": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CX": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "CZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DJ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "DZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "EC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "EE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "EG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "EH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ER": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ES": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ET": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FJ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "FR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GB": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GP": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GQ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GS": {
        "alpha2": string;
        "countryCallingCodes": any[];
        "name": string;
    };
    "GT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "GY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "HK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "HM": {
        "alpha2": string;
        "countryCallingCodes": any[];
        "name": string;
    };
    "HN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "HR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "HT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ID": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IQ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "IT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "JE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "JM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "JO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "JP": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KP": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "KZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LB": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LV": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "LY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ME": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ML": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MP": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MQ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MV": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MX": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "MZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NP": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "NZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "OM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "PY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "QA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "RE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "RO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "RS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "RU": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "RW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SB": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SJ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ST": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SV": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SX": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "SZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TD": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TF": {
        "alpha2": string;
        "countryCallingCodes": any[];
        "name": string;
    };
    "TG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TH": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TJ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TK": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TL": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TO": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TR": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TV": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "TZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "UA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "UG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "UM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "US": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "UY": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "UZ": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VC": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VG": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VI": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "VN": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "WF": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "WS": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "YE": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "YT": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ZA": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ZM": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
    "ZW": {
        "alpha2": string;
        "countryCallingCodes": string[];
        "name": string;
    };
};
