import { ModuleWithProviders } from "@angular/core";
export declare class DevelluxSharedModule {
    static forRoot(config?: {
        apiUrl: string;
    }): ModuleWithProviders;
}
