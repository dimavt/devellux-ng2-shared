"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var transaction_state_renderer_component_1 = require("../renderers/transaction-state-renderer.component");
var to_now_pipe_1 = require("../pipes/to-now.pipe");
var from_now_pipe_1 = require("../pipes/from-now.pipe");
var order_state_renderer_component_1 = require("../renderers/order-state-renderer.component");
var facebook_button_directive_1 = require("../directives/social_buttons/facebook_button/facebook-button.directive");
var google_button_directive_1 = require("../directives/social_buttons/google_button/google-button.directive");
var form_errors_component_1 = require("../helpers/form/form-errors.component");
var keys_pipe_1 = require("../pipes/keys.pipe");
var to_money_pipe_1 = require("../pipes/to-money.pipe");
var client_1 = require("../helpers/http/client");
var request_options_1 = require("../helpers/http/request-options");
var forms_1 = require("@angular/forms");
var dropdown_component_1 = require("../components/custom_inputs/dropdown-select/dropdown.component");
var arr = [
    transaction_state_renderer_component_1.TransactionStateRendererComponent,
    to_now_pipe_1.ToNowPipe,
    from_now_pipe_1.FromNowPipe,
    order_state_renderer_component_1.OrderStateRendererComponent,
    facebook_button_directive_1.FacebookButtonDirective,
    google_button_directive_1.GoogleButtonDirective,
    form_errors_component_1.FormErrorsComponent,
    keys_pipe_1.KeysPipe,
    to_money_pipe_1.ToMoneyPipe,
    dropdown_component_1.DropdownSelectorComponent,
];
var DevelluxSharedModule = (function () {
    function DevelluxSharedModule() {
    }
    DevelluxSharedModule.forRoot = function (config) {
        if (config === void 0) { config = { apiUrl: '' }; }
        var providers = [
            {
                provide: client_1.HttpClient,
                useFactory: function (http) {
                    var httpClient = new client_1.HttpClient(http);
                    httpClient.baseUrl = config.apiUrl;
                    return httpClient;
                }, deps: [http_1.Http]
            },
            {
                provide: http_1.RequestOptions,
                useFactory: function () {
                    var appRequestOptions = new request_options_1.AppRequestOptions();
                    appRequestOptions.baseUrl = config.apiUrl;
                    return appRequestOptions;
                }
            },
        ];
        return {
            ngModule: DevelluxSharedModule,
            providers: providers,
        };
    };
    DevelluxSharedModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, http_1.HttpModule, forms_1.ReactiveFormsModule, forms_1.FormsModule],
            declarations: arr,
            exports: arr,
        }), 
        __metadata('design:paramtypes', [])
    ], DevelluxSharedModule);
    return DevelluxSharedModule;
}());
exports.DevelluxSharedModule = DevelluxSharedModule;
