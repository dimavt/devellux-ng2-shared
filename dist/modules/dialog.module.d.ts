import { Modal } from "angular2-modal/index";
export declare class DialogService {
    private modal;
    constructor(modal: Modal);
    open<CMP, DATA>(component: {
        new (...args: any[]): CMP & DATA;
    }, data: DATA): Promise<any>;
}
export declare class DialogModule {
    static forRoot(): void;
    static forChild(): void;
}
