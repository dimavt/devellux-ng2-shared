"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("angular2-modal/index");
var modal_context_1 = require("angular2-modal/plugins/bootstrap/modal-context");
var bootstrap_module_1 = require("angular2-modal/plugins/bootstrap/bootstrap.module");
var DialogService = (function () {
    function DialogService(modal) {
        this.modal = modal;
    }
    DialogService.prototype.open = function (component, data) {
        var builder = new modal_context_1.BSModalContextBuilder(data, undefined, modal_context_1.BSModalContext);
        var overlayConfig = {
            context: builder.toJSON()
        };
        return this.modal.open(component, overlayConfig).then(function (ref) {
            return ref.result;
        });
    };
    DialogService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [index_1.Modal])
    ], DialogService);
    return DialogService;
}());
exports.DialogService = DialogService;
var DialogModule = (function () {
    function DialogModule() {
    }
    DialogModule.forRoot = function () {
    };
    DialogModule.forChild = function () {
    };
    DialogModule = __decorate([
        core_1.NgModule({
            imports: [
                index_1.ModalModule,
                bootstrap_module_1.BootstrapModalModule
            ],
            providers: [
                DialogService,
                bootstrap_module_1.BootstrapModalModule.getProviders()
            ],
        }), 
        __metadata('design:paramtypes', [])
    ], DialogModule);
    return DialogModule;
}());
exports.DialogModule = DialogModule;
