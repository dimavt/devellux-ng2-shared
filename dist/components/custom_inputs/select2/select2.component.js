"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var custom_ng2_input_1 = require("../custom-ng2-input");
var select_1 = require("./deps/select");
var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: forms_1.NG_VALUE_ACCESSOR,
    useExisting: core_1.forwardRef(function () { return Select2Component; }),
    multi: true
};
var Select2Component = (function (_super) {
    __extends(Select2Component, _super);
    function Select2Component() {
        _super.apply(this, arguments);
        this.options = [];
        this.disabled = false;
        this.placeholder = 'some';
        this.items = [];
    }
    Select2Component.prototype.ngOnInit = function () {
        // convert to ng-select items
        this.items = this.options.map(function (o) { return ({ id: o.value, text: o.title }); });
    };
    Select2Component.prototype.selectOption = function (value) {
        this.value = value.id;
        this.touched();
    };
    Select2Component.prototype.onTouched = function () {
        this.touched();
    };
    Select2Component.prototype.updateViewValue = function (v) {
        var view = this.items.find(function (item) { return item.id === v; });
        if (view) {
            this.viewValue = [view];
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], Select2Component.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], Select2Component.prototype, "disabled", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], Select2Component.prototype, "placeholder", void 0);
    Select2Component = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'select2',
            template: "<ng-select [allowClear]=\"true\"\n              [items]=\"items\"\n              [active]=\"viewValue\"\n              [disabled]=\"disabled\"\n              (selected)=\"selectOption($event)\"\n              [placeholder]=\"placeholder\"\n              >\n  </ng-select>",
            // (removed)="removed($event)"
            // (typed)="typed($event)"
            // (data)="refreshValue($event)"
            directives: [select_1.SelectComponent],
            providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
        }), 
        __metadata('design:paramtypes', [])
    ], Select2Component);
    return Select2Component;
}(custom_ng2_input_1.CustomNgInput));
exports.Select2Component = Select2Component;
