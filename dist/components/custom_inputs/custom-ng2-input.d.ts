import { ControlValueAccessor } from "@angular/forms";
export declare abstract class CustomNgInput implements ControlValueAccessor {
    private _value;
    private onTouchedCallback;
    private onChangeCallback;
    protected updateViewValue(v: any): void;
    touched(): void;
    value: any;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: any): void;
}
