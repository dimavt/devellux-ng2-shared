"use strict";
var CustomNgInput = (function () {
    function CustomNgInput() {
        /* tslint:enable */
        // Placeholders for the callbacks
        this.onTouchedCallback = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
        };
        this.onChangeCallback = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
        };
    }
    CustomNgInput.prototype.updateViewValue = function (v) {
        throw "not implemented updateViewValue(v) in " + this;
    };
    ;
    CustomNgInput.prototype.touched = function () {
        this.onTouchedCallback();
    };
    Object.defineProperty(CustomNgInput.prototype, "value", {
        // get accessor
        get: function () {
            return this._value;
        },
        // set accessor including call the onchange callback
        set: function (v) {
            if (v !== this._value) {
                this._value = v;
                this.onChangeCallback(v);
                this.updateViewValue(v);
            }
        },
        enumerable: true,
        configurable: true
    });
    ;
    // From ControlValueAccessor interface
    CustomNgInput.prototype.writeValue = function (value) {
        this.value = value;
    };
    // From ControlValueAccessor interface
    CustomNgInput.prototype.registerOnChange = function (fn) {
        this.onChangeCallback = fn;
    };
    // From ControlValueAccessor interface
    CustomNgInput.prototype.registerOnTouched = function (fn) {
        this.onTouchedCallback = fn;
    };
    return CustomNgInput;
}());
exports.CustomNgInput = CustomNgInput;
