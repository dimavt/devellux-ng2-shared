import { ControlValueAccessor } from "@angular/forms";
import { CustomNgInput } from "../custom-ng2-input";
export declare class DropdownSelectorComponent extends CustomNgInput implements ControlValueAccessor {
    options: Option[];
    disabled: boolean;
    valueTitle: any;
    titleSelected: boolean;
    updateViewValue(v: any): void;
    selectOption(option: any): void;
    onTouched(): void;
}
export interface Option {
    title: any;
    value: any;
}
