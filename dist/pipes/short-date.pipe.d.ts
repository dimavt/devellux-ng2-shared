import { PipeTransform } from '@angular/core';
export declare class ShortDatePipe implements PipeTransform {
    transform(date: any): string;
}
