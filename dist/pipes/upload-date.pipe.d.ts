import { PipeTransform } from '@angular/core';
export declare class UploadDatePipe implements PipeTransform {
    transform(date: any): any;
}
