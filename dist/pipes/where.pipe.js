"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var WherePipe = (function () {
    function WherePipe() {
    }
    /**
     * Support a function or a value or the shorthand ['key', value] like the lodash shorthand.
     */
    WherePipe.prototype.transform = function (input, fn) {
        if (!Array.isArray(input)) {
            return input;
        }
        if (typeof fn === 'function') {
            return input.filter(fn);
        }
        else if (Array.isArray((fn))) {
            var key_1 = fn[0], value_1 = fn[1];
            return input.filter(function (item) { return getProperty(item, key_1) === value_1; });
        }
        else if (fn) {
            return input.filter(function (item) { return item === fn; });
        }
        else {
            return input;
        }
    };
    WherePipe = __decorate([
        core_1.Pipe({
            name: 'where'
        }), 
        __metadata('design:paramtypes', [])
    ], WherePipe);
    return WherePipe;
}());
exports.WherePipe = WherePipe;
function getProperty(value, key) {
    var keys = key.split('.');
    var result = value[keys.shift()];
    while (keys.length && (result = result[keys.shift()])) {
    }
    return result;
}
exports.getProperty = getProperty;
