import { PipeTransform } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
export declare class ToMoneyPipe implements PipeTransform {
    private cp;
    constructor(cp: CurrencyPipe);
    transform(value: any): string;
}
