import { PipeTransform } from '@angular/core';
export declare class ToNowPipe implements PipeTransform {
    transform(date: any): string;
}
