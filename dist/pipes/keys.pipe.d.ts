import { PipeTransform } from '@angular/core';
export declare class KeysPipe implements PipeTransform {
    transform(input: any): any;
}
