import { PipeTransform } from '@angular/core';
export declare class WherePipe implements PipeTransform {
    /**
     * Support a function or a value or the shorthand ['key', value] like the lodash shorthand.
     */
    transform(input: any, fn: any): any;
}
export declare function getProperty(value: Object, key: string): Array<any>;
