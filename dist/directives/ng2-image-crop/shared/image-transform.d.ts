export declare class ImageTransform {
    x: number;
    y: number;
    scale: number;
    constructor(x: number, y: number, scale: number);
    static parse(v: any): ImageTransform;
    static fromString(v: any): ImageTransform;
    toString(): string;
}
