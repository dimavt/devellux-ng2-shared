export declare class ImageTransformOrigin {
    x: number;
    y: number;
    constructor(el?: HTMLElement);
    toString(): string;
}
