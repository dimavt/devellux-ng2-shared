"use strict";
exports.CSS_PREFIXES = ['Webkit', 'Moz', 'ms'];
function vendorPrefix(prop) {
    var emptyStyles = document.createElement('div').style;
    if (prop in emptyStyles) {
        return prop;
    }
    var capProp = prop[0].toUpperCase() + prop.slice(1);
    var i = exports.CSS_PREFIXES.length;
    while (i--) {
        prop = exports.CSS_PREFIXES[i] + capProp;
        if (prop in emptyStyles) {
            return prop;
        }
    }
}
exports.vendorPrefix = vendorPrefix;
exports.CSS_TRANSFORM = vendorPrefix('transform');
exports.CSS_TRANS_ORG = vendorPrefix('transformOrigin');
exports.CSS_USERSELECT = vendorPrefix('userSelect');
function fix(v, decimalPoints) {
    return parseFloat(v).toFixed(decimalPoints || 0);
}
exports.fix = fix;
