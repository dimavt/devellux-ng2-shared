import { ElementRef, Renderer } from '@angular/core';
export declare class CropBoundaryImageDirective {
    element: ElementRef;
    renderer: Renderer;
    src: string;
    opacity: number;
    constructor(element: ElementRef, renderer: Renderer);
    getBoundingClientRect(): ClientRect;
}
