"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var CropBoundaryImageDirective = (function () {
    /* tslint:enable */
    // @Input() options: {};
    function CropBoundaryImageDirective(element, renderer) {
        this.element = element;
        this.renderer = renderer;
    }
    //
    // @HostBinding('attr.src')
    // get src(): string {
    //   return this._src;
    // }
    // @HostBinding('style.opacity')
    // get styleOpacity(): number {
    //   return this._opacity;
    // }
    CropBoundaryImageDirective.prototype.getBoundingClientRect = function () {
        return this.element.nativeElement.getBoundingClientRect();
    };
    __decorate([
        core_1.HostBinding('attr.src'), 
        __metadata('design:type', String)
    ], CropBoundaryImageDirective.prototype, "src", void 0);
    __decorate([
        core_1.HostBinding('style.opacity'), 
        __metadata('design:type', Number)
    ], CropBoundaryImageDirective.prototype, "opacity", void 0);
    CropBoundaryImageDirective = __decorate([
        core_1.Directive({
            selector: '[crop-boundary-image]',
        }), 
        __metadata('design:paramtypes', [core_1.ElementRef, core_1.Renderer])
    ], CropBoundaryImageDirective);
    return CropBoundaryImageDirective;
}());
exports.CropBoundaryImageDirective = CropBoundaryImageDirective;
