import { ElementRef, Renderer } from '@angular/core';
export declare class CropBoundaryViewportDirective {
    element: ElementRef;
    renderer: Renderer;
    width: number;
    height: number;
    constructor(element: ElementRef, renderer: Renderer);
    getBoundingClientRect(): ClientRect;
}
