"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var index_1 = require('./shared/index');
var MAX_FILE_SIZE = 2048000;
var MAX_FILES_COUNT = 10;
var URL = 'http://localhost:8081';
var Ng2FileUploaderComponent = (function () {
    function Ng2FileUploaderComponent() {
        this.uploader = new index_1.FileUploader({ url: URL });
        this.hasDropZoneOver = false;
        var uploaderOptions = {
            maxFileSize: MAX_FILE_SIZE,
            queueLimit: MAX_FILES_COUNT,
        };
        this.uploader.setOptions(uploaderOptions);
        this.uploader.onAfterAddingFile = this.fileSelected.bind(this);
    }
    Ng2FileUploaderComponent.prototype.fileOver = function (event) {
        this.hasDropZoneOver = event;
    };
    Ng2FileUploaderComponent.prototype.fileSelected = function (fileItem) {
        this.fileItem = fileItem;
        return { fileItem: fileItem };
    };
    Ng2FileUploaderComponent = __decorate([
        core_1.Component({
            selector: 'ng2-file-uploader',
            template: "<template [ngIf]=\"uploader && uploader.queue && uploader.queue.length == 0\">\n  <div class=\"drag-and-drop__block\"\n       ng2FileDrop\n       [uploader]=\"uploader\"\n       (fileOver)=\"fileOver($event)\"\n       [class.nv-file-over]=\"hasDropZoneOver\">\n    <i class=\"icon icon__drag-and-drop\"></i>\n    <div class=\"drag-and-drop__title\">Drag & Drop<br>your files here</div>\n  </div>\n  <div class=\"flex__container flex__j-center\">\n    <input type=\"file\" ng2FileSelect [uploader]=\"uploader\" />\n  </div>\n</template>\n\n<template [ngIf]=\"uploader.queue.length > 0\">\n  <h3>Files count {{uploader.queue.length}}</h3>\n</template>",
            styles: [".nv-file-over {\n  border: dotted 3px red;\n}"],
            directives: [index_1.FILE_UPLOAD_DIRECTIVES],
            encapsulation: core_1.ViewEncapsulation.None,
        }), 
        __metadata('design:paramtypes', [])
    ], Ng2FileUploaderComponent);
    return Ng2FileUploaderComponent;
}());
exports.Ng2FileUploaderComponent = Ng2FileUploaderComponent;
