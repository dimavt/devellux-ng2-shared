"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
exports.GOOGLE_APP_ID = new core_1.OpaqueToken('FacebookAppId');
var GoogleService = (function () {
    function GoogleService(appId) {
        var _this = this;
        this.appId = appId;
        if (appId) {
            // noinspection TypeScriptUnresolvedVariable
            window['googleAsyncInit'] = function () {
                gapi.load('auth2', function () {
                    // Retrieve the singleton for the GoogleAuth library and set up the client.
                    _this.googleAuthInstance = gapi.auth2.init({
                        client_id: appId,
                        cookiepolicy: 'single_host_origin',
                    });
                });
            };
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.async = true;
                js.src = 'https://apis.google.com/js/platform.js?onload=googleAsyncInit';
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'google-jssdk'));
        }
    }
    GoogleService.prototype.setGoogleToken = function (token) {
        this.googleAccessToken = token;
    };
    GoogleService.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.appId) {
                reject('no GOOGLE_APP_ID');
            }
            // this.googleAuthInstance.grantOfflineAccess({'redirect_uri': 'postmessage'}).then((res)=>console.log(res));
            _this.googleAuthInstance.signIn().then(function (result) {
                _this.setGoogleToken(result.getAuthResponse().id_token);
                resolve(result.getAuthResponse().id_token);
            }, function () {
                reject('login Google cancelled');
            });
        });
    };
    GoogleService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Optional()),
        __param(0, core_1.Inject(exports.GOOGLE_APP_ID)), 
        __metadata('design:paramtypes', [String])
    ], GoogleService);
    return GoogleService;
}());
exports.GoogleService = GoogleService;
