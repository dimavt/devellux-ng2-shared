"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var google_service_1 = require('./google.service');
var GoogleButtonDirective = (function () {
    function GoogleButtonDirective(service) {
        this.service = service;
        this.token = new core_1.EventEmitter();
    }
    GoogleButtonDirective.prototype.getFacebookToken = function () {
        var _this = this;
        this.service.getToken().then(function (token) {
            _this.token.emit(token);
        });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', core_1.EventEmitter)
    ], GoogleButtonDirective.prototype, "token", void 0);
    __decorate([
        core_1.HostListener('click'), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', []), 
        __metadata('design:returntype', void 0)
    ], GoogleButtonDirective.prototype, "getFacebookToken", null);
    GoogleButtonDirective = __decorate([
        core_1.Directive({
            selector: '[GoogleButton]',
            providers: [google_service_1.GoogleService]
        }), 
        __metadata('design:paramtypes', [google_service_1.GoogleService])
    ], GoogleButtonDirective);
    return GoogleButtonDirective;
}());
exports.GoogleButtonDirective = GoogleButtonDirective;
