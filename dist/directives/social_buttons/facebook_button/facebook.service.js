"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require('@angular/core');
exports.FACEBOOK_APP_ID = new core_1.OpaqueToken('FacebookAppId');
var FacebookService = (function () {
    function FacebookService(appId) {
        var _this = this;
        this.appId = appId;
        if (!appId && core_1.isDevMode()) {
            console.error('no FACEBOOK_APP_ID provided');
            return;
        }
        // noinspection TypeScriptUnresolvedVariable
        window['fbAsyncInit'] = function () {
            // Executed when the SDK is loaded
            FB.init({
                /*
                 The app id of the web app;
                 To register a new app visit Facebook App Dashboard
                 ( https://developers.facebook.com/apps/ )
                 */
                appId: appId,
                /*
                 Adding a Channel File improves the performance
                 of the javascript SDK, by addressing issues
                 with cross-domain communication in certain browsers.
                 */
                // channelUrl: 'app/channel.html',
                /*
                 Set if you want to check the authentication status
                 at the start up of the app
                 */
                status: true,
                /*
                 Enable cookies to allow the server to access
                 the session
                 */
                cookie: true,
                /* Parse XFBML */
                xfbml: true,
                version: 'v2.5'
            });
            FB.Event.subscribe('auth.statusChange', function (res) {
                if (res.status === 'connected') {
                    _this.setToken(res.authResponse.accessToken);
                }
                else {
                }
            });
        };
        // attach script
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.async = true;
            js.src = '//connect.facebook.net/en_US/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
    FacebookService.prototype.setToken = function (fbToken) {
        this.accessToken = fbToken;
    };
    FacebookService.prototype.getToken = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!_this.appId && core_1.isDevMode()) {
                reject('No FACEBOOK_APP_ID provided');
            }
            FB.login(function (response) {
                // noinspection TypeScriptUnresolvedVariable
                if (response.authResponse) {
                    resolve(response.authResponse.accessToken);
                }
                else {
                    reject('Login FB cancelled');
                }
            }, { scope: 'public_profile,email' });
        });
    };
    FacebookService = __decorate([
        core_1.Injectable(),
        __param(0, core_1.Optional()),
        __param(0, core_1.Inject(exports.FACEBOOK_APP_ID)), 
        __metadata('design:paramtypes', [String])
    ], FacebookService);
    return FacebookService;
}());
exports.FacebookService = FacebookService;
