import { OpaqueToken } from '@angular/core';
export declare let FACEBOOK_APP_ID: OpaqueToken;
export declare class FacebookService {
    private appId;
    private accessToken;
    constructor(appId: string);
    setToken(fbToken: any): void;
    getToken(): Promise<string>;
}
