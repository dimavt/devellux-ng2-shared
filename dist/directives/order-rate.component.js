"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var OrderRateComponent = (function () {
    function OrderRateComponent() {
    }
    Object.defineProperty(OrderRateComponent.prototype, "orderRate", {
        get: function () {
            if (this.rate !== 0 && !this.rate) {
                return '-';
            }
            switch (this.rate) {
                case 0:
                    return '0/5';
                case 1:
                    return '1/5';
                case 2:
                    return '2/5';
                case 3:
                    return '3/5';
                case 4:
                    return '4/5';
                case 5:
                    return '5/5';
            }
        },
        set: function (rate) {
            this.rate = rate;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], OrderRateComponent.prototype, "orderRate", null);
    OrderRateComponent = __decorate([
        core_1.Component({
            /* tslint:disable */
            selector: 'span[orderRate]',
            /* tslint:enable*/
            template: "\n  <!--<span [class]=\"getStateClass()\"><ng-content></ng-content>{{orderState}}</span>-->\n  \n    <span \n      [class.text-color__danger]=\"orderRate == '0/5' || orderRate == '1/5' || orderRate == '3/5'\"\n      [class.text-color__warning]=\"orderRate == '2/5' || orderRate == '3/5'\"\n      [class.text-color__success]=\"orderRate == '4/5' || orderRate == '5/5'\"\n    >{{orderRate}}</span>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], OrderRateComponent);
    return OrderRateComponent;
}());
exports.OrderRateComponent = OrderRateComponent;
