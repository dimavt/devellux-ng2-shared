"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./common/index'));
var common_dialog_config_1 = require('./common-dialog-config');
exports.CommonDialogConfig = common_dialog_config_1.CommonDialogConfig;
