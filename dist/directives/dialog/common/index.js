"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
__export(require('./modal'));
__export(require('./modal-container'));
__export(require('./modal-context'));
var core_1 = require('@angular/core');
var angular2_modal_1 = require('angular2-modal');
var modal_backdrop_1 = require('./modal-backdrop');
var modal_2 = require('./modal');
exports.COMMON_MODAL_PROVIDERS = angular2_modal_1.MODAL_PROVIDERS.concat([
    core_1.provide(modal_2.Modal, { useClass: modal_2.Modal }),
    core_1.provide(angular2_modal_1.ModalBackdropComponent, { useValue: modal_backdrop_1.CommonModalBackdropComponent })
]);
