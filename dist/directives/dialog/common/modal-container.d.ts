import { ComponentResolver, AfterViewInit } from '@angular/core';
import { ModalCompileConfig } from 'angular2-modal/models/tokens';
import { DialogRef } from 'angular2-modal/models/dialog-ref';
import { Modal } from './modal';
import { CommonModalContext } from './modal-context';
export declare class CommonModalContainerComponent implements AfterViewInit {
    dialog: DialogRef<CommonModalContext>;
    private compileConfig;
    private modal;
    private cr;
    position: string;
    private viewContainer;
    constructor(dialog: DialogRef<CommonModalContext>, compileConfig: ModalCompileConfig, modal: Modal, cr: ComponentResolver);
    ngAfterViewInit(): void;
    onClickOutside(): void;
    documentKeypress(event: KeyboardEvent): void;
    readonly getTabindex: number;
    readonly getStyleDisplay: string;
    readonly getStyleMargin: string;
}
