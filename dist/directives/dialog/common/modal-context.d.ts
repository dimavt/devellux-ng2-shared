import { ModalOpenContext, ModalOpenContextBuilder } from 'angular2-modal/models/modal-open-context';
import { FluentAssignMethod } from 'angular2-modal/framework/fluent-assign';
export declare class CommonModalContext extends ModalOpenContext {
    /**
     * A Class for the modal dialog container.
     * Default: modal-dialog
     */
    dialogClass: string;
    /**
     * When true, show a close button on the top right corner.
     */
    showClose: boolean;
    normalize(): void;
}
export declare class CommonModalContextBuilder<T extends CommonModalContext> extends ModalOpenContextBuilder<T> {
    /**
     * A Class for the modal dialog container.
     * Default: modal-dialog
     */
    dialogClass: FluentAssignMethod<string, this>;
    /**
     * When true, show a close button on the top right corner.
     */
    showClose: FluentAssignMethod<boolean, this>;
    constructor(defaultValues?: T, initialSetters?: string[], baseType?: new () => T);
}
