"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var tokens_1 = require('angular2-modal/models/tokens');
var dialog_ref_1 = require('angular2-modal/models/dialog-ref');
var modal_1 = require('./modal');
var CommonModalContainerComponent = (function () {
    function CommonModalContainerComponent(dialog, compileConfig, modal, cr) {
        this.dialog = dialog;
        this.compileConfig = compileConfig;
        this.modal = modal;
        this.cr = cr;
        this.position = null;
    }
    CommonModalContainerComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.cr.resolveComponent(this.compileConfig.component)
            .then(function (cmpFactory) {
            var vcr = _this.viewContainer, bindings = _this.compileConfig.bindings, ctxInjector = vcr.parentInjector;
            var childInjector = Array.isArray(bindings) && bindings.length > 0 ?
                core_1.ReflectiveInjector.fromResolvedProviders(bindings, ctxInjector) : ctxInjector;
            return _this.dialog.contentRef =
                vcr.createComponent(cmpFactory, vcr.length, childInjector);
        });
    };
    CommonModalContainerComponent.prototype.onClickOutside = function () {
        return this.modal.isTopMost(this.dialog) && !this.dialog.context.isBlocking && this.dialog.dismiss();
    };
    CommonModalContainerComponent.prototype.documentKeypress = function (event) {
        if (!this.modal.isTopMost(this.dialog)) {
            return;
        }
        if (event.keyCode === 27) {
            this.dialog.dismiss();
        }
    };
    Object.defineProperty(CommonModalContainerComponent.prototype, "getTabindex", {
        get: function () {
            return -1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalContainerComponent.prototype, "getStyleDisplay", {
        get: function () {
            return 'block';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalContainerComponent.prototype, "getStyleMargin", {
        get: function () {
            return 'auto';
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.ViewChild('modalDialog', { read: core_1.ViewContainerRef }), 
        __metadata('design:type', core_1.ViewContainerRef)
    ], CommonModalContainerComponent.prototype, "viewContainer", void 0);
    __decorate([
        core_1.HostListener('body:keydown', ['$event']), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', [KeyboardEvent]), 
        __metadata('design:returntype', void 0)
    ], CommonModalContainerComponent.prototype, "documentKeypress", null);
    __decorate([
        core_1.HostBinding('attr.tabindex'), 
        __metadata('design:type', Number)
    ], CommonModalContainerComponent.prototype, "getTabindex", null);
    __decorate([
        core_1.HostBinding('style.display'), 
        __metadata('design:type', String)
    ], CommonModalContainerComponent.prototype, "getStyleDisplay", null);
    __decorate([
        core_1.HostBinding('style.margin'), 
        __metadata('design:type', String)
    ], CommonModalContainerComponent.prototype, "getStyleMargin", null);
    CommonModalContainerComponent = __decorate([
        core_1.Component({
            selector: 'modal-container',
            encapsulation: core_1.ViewEncapsulation.None,
            template: "\n      <div (clickOutside)=\"onClickOutside()\">\n        <div style=\"display: none\" #modalDialog></div>\n      </div>\n  "
        }), 
        __metadata('design:paramtypes', [dialog_ref_1.DialogRef, tokens_1.ModalCompileConfig, modal_1.Modal, core_1.ComponentResolver])
    ], CommonModalContainerComponent);
    return CommonModalContainerComponent;
}());
exports.CommonModalContainerComponent = CommonModalContainerComponent;
