"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var modal_open_context_1 = require('angular2-modal/models/modal-open-context');
var utils_1 = require('angular2-modal/framework/utils');
var DEFAULT_VALUES = {
    dialogClass: 'type-1',
    showClose: false
};
var DEFAULT_SETTERS = [
    'dialogClass',
    'size',
    'showClose'
];
var CommonModalContext = (function (_super) {
    __extends(CommonModalContext, _super);
    function CommonModalContext() {
        _super.apply(this, arguments);
    }
    CommonModalContext.prototype.normalize = function () {
        if (!this.dialogClass) {
            this.dialogClass = DEFAULT_VALUES.dialogClass;
        }
        _super.prototype.normalize.call(this);
    };
    return CommonModalContext;
}(modal_open_context_1.ModalOpenContext));
exports.CommonModalContext = CommonModalContext;
var CommonModalContextBuilder = (function (_super) {
    __extends(CommonModalContextBuilder, _super);
    function CommonModalContextBuilder(defaultValues, initialSetters, baseType) {
        if (defaultValues === void 0) { defaultValues = undefined; }
        if (initialSetters === void 0) { initialSetters = undefined; }
        if (baseType === void 0) { baseType = undefined; }
        _super.call(this, utils_1.extend(DEFAULT_VALUES, defaultValues || {}), utils_1.arrayUnion(DEFAULT_SETTERS, initialSetters || []), baseType || CommonModalContext);
    }
    return CommonModalContextBuilder;
}(modal_open_context_1.ModalOpenContextBuilder));
exports.CommonModalContextBuilder = CommonModalContextBuilder;
