import { Modal as BaseModal } from 'angular2-modal/providers/modal';
export interface CommonModal extends BaseModal {
}
export declare const Modal: typeof BaseModal;
export declare type Modal = CommonModal;
