import { OnDestroy } from '@angular/core';
export declare class CommonModalBackdropComponent implements OnDestroy {
    constructor();
    readonly stylePosition: string;
    readonly styleHeightn: string;
    readonly styleWidth: string;
    readonly getStyleDisplay: string;
    readonly getStyleTop: string;
    readonly getStyleLeft: string;
    readonly getStyleRight: string;
    readonly getStyleBottom: string;
    readonly getBackgroundColor: string;
    ngOnDestroy(): void;
}
