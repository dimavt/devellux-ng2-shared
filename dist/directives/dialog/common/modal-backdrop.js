"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var modal_container_1 = require('./modal-container');
var dialogRefCount = 0;
var CommonModalBackdropComponent = (function () {
    function CommonModalBackdropComponent() {
        dialogRefCount++;
        document.body.classList.add('modal-open');
    }
    Object.defineProperty(CommonModalBackdropComponent.prototype, "stylePosition", {
        get: function () {
            return 'fixed';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "styleHeightn", {
        get: function () {
            return '100vh';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "styleWidth", {
        get: function () {
            return '100vw';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getStyleDisplay", {
        get: function () {
            return 'flex';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getStyleTop", {
        get: function () {
            return '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getStyleLeft", {
        get: function () {
            return '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getStyleRight", {
        get: function () {
            return '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getStyleBottom", {
        get: function () {
            return '0';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CommonModalBackdropComponent.prototype, "getBackgroundColor", {
        get: function () {
            return 'rgba(0, 0, 0, 0.48)';
        },
        enumerable: true,
        configurable: true
    });
    CommonModalBackdropComponent.prototype.ngOnDestroy = function () {
        if (--dialogRefCount === 0) {
            document.body.classList.remove('modal-open');
        }
    };
    __decorate([
        core_1.HostBinding('style.position'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "stylePosition", null);
    __decorate([
        core_1.HostBinding('style.height'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "styleHeightn", null);
    __decorate([
        core_1.HostBinding('style.width'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "styleWidth", null);
    __decorate([
        core_1.HostBinding('style.display'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getStyleDisplay", null);
    __decorate([
        core_1.HostBinding('style.top'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getStyleTop", null);
    __decorate([
        core_1.HostBinding('style.left'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getStyleLeft", null);
    __decorate([
        core_1.HostBinding('style.right'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getStyleRight", null);
    __decorate([
        core_1.HostBinding('style.bottom'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getStyleBottom", null);
    __decorate([
        core_1.HostBinding('style.backgroundColor'), 
        __metadata('design:type', String)
    ], CommonModalBackdropComponent.prototype, "getBackgroundColor", null);
    CommonModalBackdropComponent = __decorate([
        core_1.Component({
            selector: 'modal-backdrop',
            directives: [modal_container_1.CommonModalContainerComponent],
            encapsulation: core_1.ViewEncapsulation.None,
            template: "\n    <modal-container></modal-container>\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], CommonModalBackdropComponent);
    return CommonModalBackdropComponent;
}());
exports.CommonModalBackdropComponent = CommonModalBackdropComponent;
