"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var angular2_modal_1 = require('angular2-modal');
var CommonDialogConfig = (function (_super) {
    __extends(CommonDialogConfig, _super);
    function CommonDialogConfig() {
        _super.apply(this, arguments);
    }
    return CommonDialogConfig;
}(angular2_modal_1.ModalOpenContext));
exports.CommonDialogConfig = CommonDialogConfig;
