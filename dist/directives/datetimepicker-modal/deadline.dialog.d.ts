import { ModalComponent, DialogRef } from 'angular2-modal';
import { FormControl } from "@angular/forms";
export declare class DeadlineDialogComponent implements ModalComponent<any> {
    dialog: DialogRef<any>;
    context: any;
    deadline: Date;
    deadlineControl: FormControl;
    constructor(dialog: DialogRef<any>);
    readonly min: string;
    onDeadlineSelected($event: Event): void;
}
