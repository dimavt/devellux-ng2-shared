"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var angular2_modal_1 = require('angular2-modal');
var moment = require('moment');
var ng2_datetime_picker_component_1 = require('../ng2-datetime-picker/ng2-datetime-picker.component');
var forms_1 = require("@angular/forms");
var DeadlineDialogComponent = (function () {
    function DeadlineDialogComponent(dialog) {
        this.dialog = dialog;
        if (dialog.context.deadline) {
            this.deadline = moment.utc(dialog.context.deadline).toDate();
        }
        else {
            var deadline = moment(moment.utc(new Date()));
            this.deadline = deadline.add(2, 'days').toDate();
        }
        this.deadlineControl = new forms_1.FormControl(this.deadline);
    }
    Object.defineProperty(DeadlineDialogComponent.prototype, "min", {
        get: function () {
            if (moment().add({ minutes: 30 }) < moment().endOf('day')) {
                return moment().toDate().toString();
            }
            else {
                return moment().add(1, 'day').startOf('day').toDate().toString();
            }
        },
        enumerable: true,
        configurable: true
    });
    DeadlineDialogComponent.prototype.onDeadlineSelected = function ($event) {
        $event.preventDefault();
        this.dialog.close(this.deadlineControl.value);
    };
    DeadlineDialogComponent = __decorate([
        core_1.Component({
            selector: 'dialog-custom',
            directives: [ng2_datetime_picker_component_1.DatetimePickerComponent, forms_1.REACTIVE_FORM_DIRECTIVES],
            template: "\n    <ng2-datetime-picker *ngIf=\"deadline\" [formControl]=\"deadlineControl\" [min]=\"min\"></ng2-datetime-picker>\n    <div class=\"after-datepicker\">\n        <button type=\"button\" (click)=\"onDeadlineSelected($event)\">Select</button>\n        <button type=\"button\" (click)=\"dialog.close($event)\">Close</button>\n    </div>\n  "
        }), 
        __metadata('design:paramtypes', [angular2_modal_1.DialogRef])
    ], DeadlineDialogComponent);
    return DeadlineDialogComponent;
}());
exports.DeadlineDialogComponent = DeadlineDialogComponent;
