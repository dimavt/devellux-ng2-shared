"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var moment = require("moment");
var deadline_dialog_1 = require("./deadline.dialog");
var custom_ng2_input_1 = require("../../components/custom_inputs/custom-ng2-input");
var dialog_module_1 = require("../../modules/dialog.module");
/* tslint:disable */
var noop = function () {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i - 0] = arguments[_i];
    }
};
// const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = new Provider(
//   NG_VALUE_ACCESSOR, {
//     useExisting: forwardRef(() => DatetimePickerModalComponent),
//     multi: true
//   });
var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = { provide: forms_1.NG_VALUE_ACCESSOR, useExisting: core_1.forwardRef(function () { return DatetimePickerModalComponent; }), multi: true };
/* tslint:enable */
var DatetimePickerModalComponent = (function (_super) {
    __extends(DatetimePickerModalComponent, _super);
    function DatetimePickerModalComponent(modal) {
        _super.call(this);
        this.modal = modal;
        this.valueView = '';
    }
    DatetimePickerModalComponent.prototype.updateViewValue = function (v) {
        if (v) {
            this.valueView = moment(v).format('dddd, DD MMM, h:mm a');
        }
    };
    DatetimePickerModalComponent.prototype.onDeadlineClick = function () {
        var _this = this;
        this.modal.open(deadline_dialog_1.DeadlineDialogComponent, { deadline: this.value })
            .then(function (deadline) {
            if (deadline) {
                _this.value = moment(moment.utc(deadline)).format();
            }
        });
    };
    __decorate([
        core_1.HostListener('mousedown'), 
        __metadata('design:type', Function), 
        __metadata('design:paramtypes', []), 
        __metadata('design:returntype', void 0)
    ], DatetimePickerModalComponent.prototype, "onDeadlineClick", null);
    DatetimePickerModalComponent = __decorate([
        core_1.Component({
            selector: 'datetimepicker-modal',
            template: "<input type=\"text\" class=\"form-input datepicker-required\" placeholder=\"Deadline\" [(ngModel)]=\"valueView\" readonly=\"readonly\" />",
            styles: ["\n\n\n    :host.ng-invalid .datepicker-required {\n      border-color: #ef4773 !important;\n    }\n    \n    :host.ng-pristine.ng-invalid .datepicker-required {\n      border-color: #cad3d1 !important;\n    }\n    \n    :host.ng-dirty.ng-nvalid .datepicker-required {\n      border-color: #cad3d1 !important;\n    }\n    \n    :host.ng-dirty.ng-valid .datepicker-required {\n      border-color: #04be5b !important;\n    }\n    \n    \n    \n    \n  "],
            providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR],
        }), 
        __metadata('design:paramtypes', [dialog_module_1.DialogService])
    ], DatetimePickerModalComponent);
    return DatetimePickerModalComponent;
}(custom_ng2_input_1.CustomNgInput));
exports.DatetimePickerModalComponent = DatetimePickerModalComponent;
