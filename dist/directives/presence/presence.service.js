"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Rx_1 = require('rxjs/Rx');
var see_service_1 = require('../../services/see.service');
var client_1 = require("../../helpers/http/client");
var common_1 = require("../../helpers/common/common");
var PresenceService = (function () {
    function PresenceService(sse, http) {
        var _this = this;
        this.sse = sse;
        this.http = http;
        this.cache = {};
        this.userStatusChangeEE = new core_1.EventEmitter();
        this.userStatusChange$ = this.userStatusChangeEE.asObservable();
        this.sse.connect('status')
            .subscribe(function (res) {
            _this.changeUserStatus(res._id, res.online);
        });
    }
    PresenceService.prototype.isUserOnline = function (userId) {
        return this.getUserStatus(userId);
    };
    PresenceService.prototype.changeUserStatus = function (id, status) {
        this.cache[id] = status;
        this.userStatusChangeEE.emit({
            id: id, status: status
        });
    };
    PresenceService.prototype.getUserStatus = function (userId) {
        var _this = this;
        if (userId in this.cache) {
            return Rx_1.Observable.of(this.cache[userId]);
        }
        return this.http.post('/sse/subscribe', { _id: userId })
            .map(common_1.commonResponseHandler)
            .map(function (res) { return res.online; })
            .do(function (status) { return _this.changeUserStatus(userId, status); });
    };
    PresenceService.prototype.randomStatusChange = function () {
        // Random Key
        var id = Object.keys(this.cache)[Math.floor(Math.random() * Object.keys(this.cache).length)];
        if (id) {
            var status_1 = Math.random() >= 0.5;
            this.changeUserStatus(id, status_1);
        }
        // setTimeout(() => this.randomStatusChange(), 5000);
    };
    PresenceService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [see_service_1.SSEService, client_1.HttpClient])
    ], PresenceService);
    return PresenceService;
}());
exports.PresenceService = PresenceService;
