"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var presence_service_1 = require('./presence.service');
/**
 * Навешивает класс в зависимости от присутствия пользователя на сайте.
 * Классы по умолчанию 'status__online'  и 'status__offline'.
 * Если мы сразу можем передать состояние пользователя через атрибут presenceDefault.
 *
 * ### Example
 *
 * ```html
 * <div [presence]="someUser._id"></div>
 *
 * <div [presence]="someOtherUser._id"
 *      presenceOnline="custom_online"
 *      presenceOffline="mycustom_offline"
 *      [presenceDefault]="someOtherUser.currentStatus"></div>
 * ```
 *
 */
var PresenceDirective = (function () {
    function PresenceDirective(service, el, renderer) {
        this.service = service;
        this.el = el;
        this.renderer = renderer;
        this.presenceOnline = 'status__online';
        this.presenceOffline = 'status__offline';
        this.presenceDefault = false;
    }
    PresenceDirective.prototype.ngOnInit = function () {
        var _this = this;
        this.setStatus(this.presenceDefault);
        this.getStateSub = this.service.isUserOnline(this.presence)
            .subscribe(function (status) { return _this.setStatus(status); });
        this.obsStateSub = this.service.userStatusChange$
            .subscribe(function (obj) { return _this.updatesListener(obj); });
    };
    PresenceDirective.prototype.ngOnDestroy = function () {
        this.getStateSub.unsubscribe();
        this.obsStateSub.unsubscribe();
    };
    PresenceDirective.prototype.setStatus = function (status) {
        this.renderer.setElementClass(this.el.nativeElement, this.presenceOnline, status);
        this.renderer.setElementClass(this.el.nativeElement, this.presenceOffline, !status);
    };
    PresenceDirective.prototype.updatesListener = function (obj) {
        if (this.presence === obj.id) {
            this.setStatus(obj.status);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], PresenceDirective.prototype, "presence", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PresenceDirective.prototype, "presenceOnline", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], PresenceDirective.prototype, "presenceOffline", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], PresenceDirective.prototype, "presenceDefault", void 0);
    PresenceDirective = __decorate([
        core_1.Directive({
            selector: '[presence]'
        }), 
        __metadata('design:paramtypes', [presence_service_1.PresenceService, core_1.ElementRef, core_1.Renderer])
    ], PresenceDirective);
    return PresenceDirective;
}());
exports.PresenceDirective = PresenceDirective;
