import { Renderer, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { PresenceService } from './presence.service';
/**
 * Навешивает класс в зависимости от присутствия пользователя на сайте.
 * Классы по умолчанию 'status__online'  и 'status__offline'.
 * Если мы сразу можем передать состояние пользователя через атрибут presenceDefault.
 *
 * ### Example
 *
 * ```html
 * <div [presence]="someUser._id"></div>
 *
 * <div [presence]="someOtherUser._id"
 *      presenceOnline="custom_online"
 *      presenceOffline="mycustom_offline"
 *      [presenceDefault]="someOtherUser.currentStatus"></div>
 * ```
 *
 */
export declare class PresenceDirective implements OnInit, OnDestroy {
    private service;
    private el;
    private renderer;
    presence: any;
    presenceOnline: string;
    presenceOffline: string;
    presenceDefault: boolean;
    private getStateSub;
    private obsStateSub;
    constructor(service: PresenceService, el: ElementRef, renderer: Renderer);
    ngOnInit(): void;
    ngOnDestroy(): void;
    setStatus(status: any): void;
    private updatesListener(obj);
}
