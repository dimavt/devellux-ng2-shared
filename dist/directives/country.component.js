"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var CountryComponent = (function () {
    function CountryComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], CountryComponent.prototype, "short", void 0);
    CountryComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'country-cmp',
            template: "<span *ngIf=\"short\" class=\"{{'flag-icon flag-icon-' + short.toLowerCase()}}\" style=\"margin-right: 5px\">\n              </span><span class=\"country-name weight-400\">{{short || '-'}}</span>"
        }), 
        __metadata('design:paramtypes', [])
    ], CountryComponent);
    return CountryComponent;
}());
exports.CountryComponent = CountryComponent;
