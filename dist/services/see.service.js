"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var of_1 = require('rxjs/observable/of');
var fromEvent_1 = require('rxjs/observable/fromEvent');
var user_service_1 = require('./user.service');
var SSEService = (function () {
    function SSEService(user) {
        var _this = this;
        this.user = user;
        this.createSource();
        // переподключаемся при изменении статуса пользователя
        this.user.status$.subscribe(function () { return _this.createSource(); });
    }
    SSEService.prototype.connect = function (eventName) {
        if (!Object.prototype.hasOwnProperty.call(this.observables, eventName)) {
            this.observables[eventName] = fromEvent_1.fromEvent(this.source, eventName)
                .map(function (e) { return JSON.parse(e.data); })
                .catch(function (er) {
                console.error(er);
                return of_1.of({});
            });
        }
        return this.observables[eventName];
    };
    SSEService.prototype.createSource = function () {
        if (this.source) {
            this.source.close();
        }
        this.clearObservables();
        if (this.user && this.user.isAuthenticated()) {
            this.source = new EventSource("http://backend.office:8999/?auth_token=" + this.user.token);
        }
        else {
            this.source = new EventSource("http://backend.office:8999/"); // Токен - опционально
        }
        // todo написать прокси чтоб можно было передать куки на sse сервер
        // this.source = new EventSource(`http://192.168.1.13:8000`, {withCredentials: true});
    };
    SSEService.prototype.clearObservables = function () {
        this.observables = {};
    };
    SSEService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [user_service_1.User])
    ], SSEService);
    return SSEService;
}());
exports.SSEService = SSEService;
