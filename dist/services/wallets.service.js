"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
var user_service_1 = require("./user.service");
var client_1 = require("../helpers/http/client");
var common_1 = require("../helpers/common/common");
var WalletsService = (function () {
    function WalletsService(httpClient, user) {
        var _this = this;
        this.httpClient = httpClient;
        this.user = user;
        this.walletsEE = new core_1.EventEmitter();
        this.wallets$ = this.walletsEE.asObservable();
        // При изменении статуса пользователя обновляем кошельки
        this.user.status$.subscribe(function () { return _this.getWallets(); });
    }
    WalletsService.prototype.getWallets = function () {
        var _this = this;
        var obs;
        if (this.user.isAuthenticated()) {
            obs = this.httpClient.get('billing/wallet')
                .map(common_1.commonResponseListHandler)
                .catch(common_1.commonErrorHandler);
        }
        else {
            obs = Observable_1.Observable.of([]);
        }
        return obs.do(function (wallets) { return _this.walletsEE.emit(wallets); });
    };
    WalletsService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [client_1.HttpClient, user_service_1.User])
    ], WalletsService);
    return WalletsService;
}());
exports.WalletsService = WalletsService;
