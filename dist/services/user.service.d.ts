import { EventEmitter } from "@angular/core";
export declare class User {
    private token_;
    private refreshToken_;
    private data_;
    status$: EventEmitter<boolean>;
    data$: EventEmitter<any>;
    constructor();
    isAuthenticated(): boolean;
    getId(): string;
    data: any;
    token: any;
    readonly refreshToken: any;
    logout(): void;
}
