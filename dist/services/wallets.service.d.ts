import { Observable } from "rxjs/Observable";
import { User } from "./user.service";
import { HttpClient } from "../helpers/http/client";
export declare class WalletsService implements WalletsServiceInterface {
    private httpClient;
    private user;
    private walletsEE;
    wallets$: Observable<Wallet[]>;
    constructor(httpClient: HttpClient, user: User);
    getWallets(): Observable<Wallet[]>;
}
export interface WalletsServiceInterface {
    wallets$: Observable<Wallet[]>;
    getWallets(): Observable<Wallet[]>;
}
export interface Wallet {
    balance: number;
    restrictions: Restrictions;
    _id: string;
    currency: 'USD';
}
export interface Restrictions {
    refund: boolean;
    pay: boolean;
    deposit: boolean;
    accept: boolean;
    withdrawal: boolean;
}
