import { Observable } from 'rxjs/Observable';
import { User } from './user.service';
export declare class SSEService {
    private user;
    private source;
    private observables;
    constructor(user: User);
    connect(eventName: string): Observable<any>;
    private createSource();
    private clearObservables();
}
