"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var jwt_1 = require("../helpers/jwt/jwt");
var cookie_1 = require("../helpers/cookie/cookie");
var User = (function () {
    function User() {
        var _this = this;
        this.status$ = new core_1.EventEmitter();
        this.data$ = new core_1.EventEmitter();
        window.addEventListener('storage', function (event) {
            if (event.key === 'data' && event.newValue === null) {
                console.log('succesful logout');
                _this.logout();
            }
        });
    }
    User.prototype.isAuthenticated = function () {
        return !!this.token;
    };
    User.prototype.getId = function () {
        if (this.data) {
            return this.data._id;
        }
        return '';
    };
    Object.defineProperty(User.prototype, "data", {
        get: function () {
            var _this = this;
            if (!this.isAuthenticated() && this.data_) {
                Object.keys(this.data_).forEach(function (key) {
                    delete _this.data_[key];
                });
                this.data_ = null;
            }
            if (this.data_) {
                return this.data_;
            }
            if (localStorage.getItem('data')) {
                this.data_ = JSON.parse(localStorage.getItem('data'));
                return this.data_;
            }
            if (this.token) {
                this.data_ = jwt_1.JwtHelper.decodeToken(this.token_);
                return this.data_;
            }
            return {};
        },
        set: function (value) {
            // TODO rewrite to assign
            // Object.assign(this.data_, value);
            if (!this.isAuthenticated()) {
                return;
            }
            if (value !== undefined && value !== null) {
                for (var nextKey in value) {
                    if (value.hasOwnProperty(nextKey)) {
                        this.data_[nextKey] = value[nextKey];
                    }
                }
            }
            try {
                localStorage.setItem('data', JSON.stringify(this.data_));
            }
            catch (er) {
                console.error('To hell with him', er);
            }
            this.data$.emit(this.data_);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "token", {
        get: function () {
            if (!this.token_) {
                this.token_ = cookie_1.getCookie('token');
            }
            return this.token_;
        },
        set: function (value) {
            this.token_ = value;
            try {
                cookie_1.setCookie('token', value, { expires: 3600 * 24 * 30 });
            }
            catch (er) {
                console.error('To hell with him', er);
            }
            this.status$.emit(this.isAuthenticated());
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "refreshToken", {
        get: function () {
            if (!this.refreshToken_) {
            }
            return this.refreshToken_;
        },
        enumerable: true,
        configurable: true
    });
    User.prototype.logout = function () {
        cookie_1.deleteCookie('token');
        localStorage.removeItem('data');
        this.token_ = null;
        delete this.token_;
        this.data_ = null;
        delete this.data_;
        this.status$.emit(false);
    };
    User = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], User);
    return User;
}());
exports.User = User;
