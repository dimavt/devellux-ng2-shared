"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var OrderStateRendererComponent = (function () {
    function OrderStateRendererComponent() {
    }
    Object.defineProperty(OrderStateRendererComponent.prototype, "orderState", {
        get: function () {
            if (!this.state) {
                return '';
            }
            switch (this.state) {
                case 'available':
                    return 'Available';
                case 'bidding':
                    return 'Bidding';
                case 'in_progress':
                    return 'In Progress';
                case 'finished':
                    return 'Finished';
                case 'cancelled':
                    return 'Canceled';
                case 'canceled':
                    return 'Canceled';
            }
        },
        set: function (state) {
            this.state = state;
        },
        enumerable: true,
        configurable: true
    });
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object), 
        __metadata('design:paramtypes', [Object])
    ], OrderStateRendererComponent.prototype, "orderState", null);
    OrderStateRendererComponent = __decorate([
        core_1.Component({
            selector: 'order-state-renderer',
            template: "\n    <span \n      [class.available]=\"orderState == 'Available'\"\n      [class.bidding]=\"orderState == 'Bidding'\"\n      [class.in_progress]=\"orderState == 'In Progress'\"\n      [class.finished]=\"orderState == 'Finished'\"\n      [class.cancelled]=\"orderState == 'Canceled'\"\n    >{{orderState}}</span>",
            styles: [".bidding{\n    color: #0082D5;\n}\n.in_progress{\n    color: #ff9948;\n}\n.finished{\n    color: #039547;\n}\n.cancelled{\n    color: #d2335c;\n}\n    /* DARK */\n:host(.dark) .bidding{\n    color: #81BBFF;\n    font-weight: 400;\n}\n:host(.dark) .in_progress{\n    color: #FFAA59;\n    font-weight: 400;\n}\n:host(.dark) .finished{\n    color: #04BE5B;\n    font-weight: 400;\n}\n:host(.dark) .cancelled{\n    color: #F56185;\n    font-weight: 400;\n}"]
        }), 
        __metadata('design:paramtypes', [])
    ], OrderStateRendererComponent);
    return OrderStateRendererComponent;
}());
exports.OrderStateRendererComponent = OrderStateRendererComponent;
