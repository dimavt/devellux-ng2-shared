export declare class TransactionStateRendererComponent {
    states: {
        new: {
            color: string;
            text: string;
        };
        closed: {
            color: string;
            text: string;
        };
        finished: {
            color: string;
            text: string;
        };
        success: {
            color: string;
            text: string;
        };
        pending: {
            color: string;
            text: string;
        };
    };
    constructor();
    state: string;
}
