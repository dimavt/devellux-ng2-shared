"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var TransactionStateRendererComponent = (function () {
    function TransactionStateRendererComponent() {
        this.states = {
            new: {
                color: '#bd10e0', text: 'New'
            },
            closed: {
                color: '#54657e', text: 'Closed'
            },
            finished: {
                color: '#00a259', text: 'Ok',
            },
            success: {
                color: '#00a259', text: 'Ok',
            },
            pending: {
                color: '#ff9948', text: 'Pending'
            }
        };
        this.state = '';
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], TransactionStateRendererComponent.prototype, "state", void 0);
    TransactionStateRendererComponent = __decorate([
        core_1.Component({
            selector: 'transaction-state-renderer',
            template: "<span style=\"text-transform: none\" [style.color]=\"states[state].color\">{{states[state].text}}</span>"
        }), 
        __metadata('design:paramtypes', [])
    ], TransactionStateRendererComponent);
    return TransactionStateRendererComponent;
}());
exports.TransactionStateRendererComponent = TransactionStateRendererComponent;
