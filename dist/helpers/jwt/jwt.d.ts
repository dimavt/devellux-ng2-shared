export declare class JwtHelper {
    static urlBase64Decode(str: string): string;
    static decodeToken(token: string): any;
    static getTokenExpirationDate(token: string): Date;
    static isTokenExpired(token: string, offsetSeconds?: number): boolean;
}
