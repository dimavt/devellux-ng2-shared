"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var http_1 = require("@angular/http");
var cookie_1 = require("../../helpers/cookie/cookie");
var AppRequestOptions = (function (_super) {
    __extends(AppRequestOptions, _super);
    function AppRequestOptions() {
        _super.apply(this, arguments);
        this.headers = new http_1.Headers({
            'Authorization': "Bearer " + cookie_1.getCookie('token'),
            'Content-Type': 'application/json',
            'X-Frontend-Url': window.location.origin,
        });
        this.withCredentials = true;
        this.baseUrl = '';
    }
    AppRequestOptions.isAbsoluteUrl = function (url) {
        var absolutePattern = /^https?:\/\//i;
        return absolutePattern.test(url);
    };
    AppRequestOptions.prototype.prepareUrl = function (url) {
        url = AppRequestOptions.isAbsoluteUrl(url) ? url : this.baseUrl + '/' + url;
        return url.replace(/([^:]\/)\/+/g, '$1');
    };
    AppRequestOptions.prototype.merge = function (options) {
        var result = new AppRequestOptions(options);
        result.baseUrl = this.baseUrl;
        result.url = this.prepareUrl(result.url);
        return result;
    };
    return AppRequestOptions;
}(http_1.RequestOptions));
exports.AppRequestOptions = AppRequestOptions;
