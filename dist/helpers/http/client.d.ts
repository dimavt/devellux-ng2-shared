import { RequestOptionsArgs, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
export declare class HttpClient {
    private http;
    baseUrl: string;
    constructor(http: Http);
    private prepareRequest(requestArgs, additionalOptions);
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
    patch(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response>;
}
