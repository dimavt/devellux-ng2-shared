import { OnInit } from "@angular/core";
import { ControlContainer } from "@angular/forms";
/**
 * <form-errors [control]="form.controls['email']" [errors]="{required: 'Email is required'}"></form-errors>
 * <form-errors control="email" [errors]="{required: 'Email is required'}"></form-errors>
 */
export declare class FormErrorsComponent implements OnInit {
    private parent;
    control: any;
    errors: any;
    ERRORS: {
        'AUTH.EMAIL_ALREADY_TAKEN': string;
        'AUTH.EMAIL_NOT_FOUND': string;
        'AUTH.WRONG_PASSWORD': string;
        isEmail: string;
        required: string;
        minlength: string;
        isMatch: string;
        pattern: string;
    };
    constructor(parent: ControlContainer);
    ngOnInit(): void;
}
