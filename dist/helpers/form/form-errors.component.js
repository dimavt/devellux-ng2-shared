"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
/**
 * <form-errors [control]="form.controls['email']" [errors]="{required: 'Email is required'}"></form-errors>
 * <form-errors control="email" [errors]="{required: 'Email is required'}"></form-errors>
 */
var FormErrorsComponent = (function () {
    function FormErrorsComponent(parent) {
        this.parent = parent;
        this.errors = {};
        this.ERRORS = {
            'AUTH.EMAIL_ALREADY_TAKEN': 'Email already taken',
            'AUTH.EMAIL_NOT_FOUND': 'Email is not registered',
            'AUTH.WRONG_PASSWORD': 'Wrong password',
            isEmail: 'Invalid email address',
            required: 'Required',
            minlength: 'Password is too short',
            isMatch: 'Passwords do not match',
            pattern: 'Not allowed symbols',
        };
    }
    FormErrorsComponent.prototype.ngOnInit = function () {
        if (typeof this.errors === "object") {
            Object.assign(this.ERRORS, this.errors);
        }
        if (!(this.control instanceof forms_1.FormControl)) {
            this.control = this.parent['form'].controls[this.control];
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Object)
    ], FormErrorsComponent.prototype, "control", void 0);
    __decorate([
        // FormControl|string
        core_1.Input(), 
        __metadata('design:type', Object)
    ], FormErrorsComponent.prototype, "errors", void 0);
    FormErrorsComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'form-errors',
            template: "\n    <div *ngIf=\"!control.pristine && !control.valid && control.touched\">\n      <template ngFor let-key [ngForOf]=\"control.errors | keys\">\n          <div class=\"auth-cmp__input-error\" role=\"alert\">\n              {{ERRORS[key]}}\n          </div>\n      </template>\n    </div>\n  ",
        }),
        __param(0, core_1.Host()),
        __param(0, core_1.SkipSelf()), 
        __metadata('design:paramtypes', [forms_1.ControlContainer])
    ], FormErrorsComponent);
    return FormErrorsComponent;
}());
exports.FormErrorsComponent = FormErrorsComponent;
