"use strict";
var Observable_1 = require('rxjs/Observable');
function formErrorHandler(form, errors) {
    if (errors && errors.error_map) {
        Object.keys(errors.error_map).forEach((function (key) {
            if (!form.contains(key)) {
                return;
            }
            var e = {};
            e[errors.error_map[key].code] = errors.error_map[key].description;
            form.controls[key].setErrors(e);
        }));
    }
}
exports.formErrorHandler = formErrorHandler;
function copyObject(object) {
    var objectCopy = {};
    for (var key in object) {
        if (object.hasOwnProperty(key)) {
            objectCopy[key] = object[key];
        }
    }
    return objectCopy;
}
exports.copyObject = copyObject;
function commonResponseHandler(response) {
    // todo check status
    var res = response.json();
    return res;
}
exports.commonResponseHandler = commonResponseHandler;
function commonResponseListHandler(response) {
    var res = response.json().objects;
    return res;
}
exports.commonResponseListHandler = commonResponseListHandler;
function commonErrorHandler(error) {
    // In a real world app, we might send the error to remote logging infrastructure
    var e;
    try {
        console.log('Request error', error.json());
        e = Observable_1.Observable.throw(error.json());
    }
    catch (error) {
        e = Observable_1.Observable.throw({ 'description': 'Server error' });
    }
    return e;
}
exports.commonErrorHandler = commonErrorHandler;
