"use strict";
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
// todo <T>  ,  func:(p:URLSearchParams)=>Observable<T|any> ,  подумать еще про механизм unsubscribe
function makeInfinite(func, opts) {
    var params = new http_1.URLSearchParams();
    Object.keys(opts).forEach(function (key) {
        if (opts[key]) {
            params.set(key, opts[key]);
        }
    });
    var skip = 0;
    var hasMore = true;
    var result = new Rx_1.Subject();
    var loadMore = function () {
        if (!hasMore) {
            return;
        }
        params.set('skip', skip.toString());
        return func(params)
            .subscribe(function (res) {
            result.next(res.objects);
            skip = skip + res.objects.length;
            hasMore = skip < res.total;
            if (!hasMore) {
                result.complete();
            }
        });
    };
    return { result: result, loadMore: loadMore };
}
exports.makeInfinite = makeInfinite;
