import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
export declare function formErrorHandler(form: any, errors: any): void;
export declare function copyObject<T>(object: T): T;
export declare function commonResponseHandler<T>(response: Response): T;
export declare function commonResponseListHandler<T>(response: Response): T[];
export declare function commonErrorHandler(error: any): Observable<any>;
export interface ListResponse<T> {
    objects: T[];
    total: number;
}
