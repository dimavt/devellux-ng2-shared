import { URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Rx";
export declare function makeInfinite(func: (p: URLSearchParams) => any, opts: any): {
    result: Observable<any>;
    loadMore: Function;
};
