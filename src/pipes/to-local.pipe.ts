import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'toLocal'
})
export class UtcToLocal implements PipeTransform {
  transform(date) {
    return moment.utc(date).toDate();
  }
}
