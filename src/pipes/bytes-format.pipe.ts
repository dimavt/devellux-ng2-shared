import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'bytesFormat',
  pure: false
})
export class BytesFormatPipe implements PipeTransform {
  transform(bytes: any, precision: number = 1): string {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) {

      return '-';
    }

    let units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'], number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
  }
}
