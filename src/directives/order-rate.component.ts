import { Component, Input } from '@angular/core';

@Component({
  /* tslint:disable */
  selector: 'span[orderRate]',
  /* tslint:enable*/
  template: `
  <!--<span [class]="getStateClass()"><ng-content></ng-content>{{orderState}}</span>-->
  
    <span 
      [class.text-color__danger]="orderRate == '0/5' || orderRate == '1/5' || orderRate == '3/5'"
      [class.text-color__warning]="orderRate == '2/5' || orderRate == '3/5'"
      [class.text-color__success]="orderRate == '4/5' || orderRate == '5/5'"
    >{{orderRate}}</span>
  `
})
export class OrderRateComponent {
  private rate;

  @Input()
  set orderRate(rate) {
    this.rate = rate;
  }

  get orderRate() {
    if (this.rate !==0 && !this.rate) {
      return '-';
    }

    switch (this.rate) {
      case 0:
        return '0/5';
      case 1:
        return '1/5';
      case 2:
        return '2/5';
      case 3:
        return '3/5';
      case 4:
        return '4/5';
      case 5:
        return '5/5';
    }
  }

}
