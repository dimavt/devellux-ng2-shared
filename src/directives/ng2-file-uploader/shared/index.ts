export { FileItem } from './file-item.class'
export { FileLikeObject } from './file-like-object.class'
export { FileDropDirective } from './file-drop.directive'
export { FileSelectDirective } from './file-select.directive'
export { FileType } from './file-type.class'
export { FileUploader, FileUploaderOptions } from './file-uploader.class'

import { FileSelectDirective } from './file-select.directive';
import { FileDropDirective } from './file-drop.directive';

export const FILE_UPLOAD_DIRECTIVES: [any] = [FileSelectDirective, FileDropDirective];

