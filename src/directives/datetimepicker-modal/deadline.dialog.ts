import { Component } from '@angular/core';
import { ModalComponent, DialogRef } from 'angular2-modal';
import * as moment from 'moment';
import { DatetimePickerComponent } from '../ng2-datetime-picker/ng2-datetime-picker.component';
import {FormControl, REACTIVE_FORM_DIRECTIVES} from "@angular/forms";



@Component({
  selector: 'dialog-custom',
  directives: [ DatetimePickerComponent, REACTIVE_FORM_DIRECTIVES ],
  template: `
    <ng2-datetime-picker *ngIf="deadline" [formControl]="deadlineControl" [min]="min"></ng2-datetime-picker>
    <div class="after-datepicker">
        <button type="button" (click)="onDeadlineSelected($event)">Select</button>
        <button type="button" (click)="dialog.close($event)">Close</button>
    </div>
  `
})
export class DeadlineDialogComponent implements ModalComponent<any> {
  context: any;
  deadline: Date;
  deadlineControl:FormControl;

  constructor(public dialog: DialogRef<any>) {
    if (dialog.context.deadline) {
      this.deadline = moment.utc(dialog.context.deadline).toDate();
    } else {
      let deadline = moment(moment.utc(new Date()));
      this.deadline = deadline.add(2, 'days').toDate();
    }

    this.deadlineControl = new FormControl(this.deadline);
  }

  get min() {
    if (moment().add({minutes:30}) < moment().endOf('day'))  {
      return moment().toDate().toString();
    } else {
      return moment().add(1, 'day').startOf('day').toDate().toString();
    }
  }

  onDeadlineSelected($event: Event) {
    $event.preventDefault();
    this.dialog.close(this.deadlineControl.value);
  }
}
