import * as Utils from './utils';

export class ImageTransform {
  x: number;
  y: number;
  scale: number;

  constructor(x: number, y: number, scale: number) {
    this.x = x;
    this.y = y;
    this.scale = scale;
  }

  static parse(v: any): ImageTransform {
    if (v.style) {
      return ImageTransform.parse(v.style[Utils.CSS_TRANSFORM]);
    } else {
      return ImageTransform.fromString(v);
    }
  }

  static fromString(v: any): ImageTransform {
    let values = v.split(') ');
    let translate = values[0].substring(12).split(',');
    let scale = values.length > 1 ? parseInt(values[1].substring(6)) : 1;
    let x = translate.length > 1 ? parseInt(translate[0].replace('px', '')) : 0;
    let y = translate.length > 1 ? parseInt(translate[1].replace('px', '')) : 0;
    return new ImageTransform(x, y, scale);
  };

  toString(): string {
    return `translate3d(${this.x}px, ${this.y}px, 0px) scale(${this.scale})`;
  };
}
