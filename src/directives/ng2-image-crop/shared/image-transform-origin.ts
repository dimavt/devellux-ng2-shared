import * as Utils from './utils';

export class ImageTransformOrigin {
  x: number;
  y: number;

  constructor(el?: HTMLElement) {
    if (!el || !el.style[Utils.CSS_TRANS_ORG]) {
      this.x = 0;
      this.y = 0;
    } else {
      var css = el.style[Utils.CSS_TRANS_ORG].split(' ');
      this.x = parseFloat(css[0]);
      this.y = parseFloat(css[1]);
    }
  }

  toString(): string {
    return `${this.x}px ${this.y}px`;
  };
}
