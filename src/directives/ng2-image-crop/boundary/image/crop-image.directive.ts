import { Directive, Input, HostBinding, ElementRef, Renderer } from '@angular/core';


@Directive({
  selector: '[crop-boundary-image]',
})
export class CropBoundaryImageDirective {
  /* tslint:disable */
  @HostBinding('attr.src')
  public src: string;
  @HostBinding('style.opacity')
  public opacity: number;
  /* tslint:enable */
  // @Input() options: {};

  constructor(public element: ElementRef, public renderer: Renderer) {
  }
  //
  // @HostBinding('attr.src')
  // get src(): string {
  //   return this._src;
  // }

  // @HostBinding('style.opacity')
  // get styleOpacity(): number {
  //   return this._opacity;
  // }

  public getBoundingClientRect(): ClientRect {
    return this.element.nativeElement.getBoundingClientRect();
  }
}
