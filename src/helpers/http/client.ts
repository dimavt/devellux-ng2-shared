import { Injectable } from '@angular/core';
import { RequestMethod, RequestOptionsArgs, Request, Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { AppRequestOptions } from './request-options';


@Injectable()
export class HttpClient {
  public baseUrl = '';
  
  constructor(private http: Http) {
  }

  private prepareRequest(requestArgs: RequestOptionsArgs, additionalOptions: RequestOptionsArgs): Observable<Response> {
    let options = new AppRequestOptions();
    options.baseUrl = this.baseUrl;

    if (additionalOptions) {
      requestArgs = Object.assign(requestArgs, additionalOptions);
    }

    options = options.merge(requestArgs);

    if (typeof options.body !== 'string') {
      options.body = JSON.stringify(options.body);
    }

    let req = new Request(options);

    return this.http.request(req);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, method: RequestMethod.Get}, options);
  }

  post(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Post}, options);
  }

  put(url: string, body?: any, options ?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Put}, options);
  }

  delete(url: string, body?: any, options ?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Delete}, options);
  }

  patch(url: string, body?: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.prepareRequest({url: url, body: body, method: RequestMethod.Patch}, options);
  }

}
