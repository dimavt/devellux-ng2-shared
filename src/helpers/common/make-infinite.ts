import {URLSearchParams} from "@angular/http";
import {Subject, Observable} from "rxjs/Rx";

// todo <T>  ,  func:(p:URLSearchParams)=>Observable<T|any> ,  подумать еще про механизм unsubscribe
export function makeInfinite(func:(p:URLSearchParams)=>any, opts:any):{result:Observable<any>, loadMore:Function} {
  let params:URLSearchParams = new URLSearchParams();
  Object.keys(opts).forEach(key => {
    if (opts[key]) {
      params.set(key, opts[key]);
    }
  });

  let skip = 0;
  let hasMore = true;
  let result = new Subject();

  let loadMore = () => {
    if (!hasMore) {
      return;
    }
    params.set('skip', skip.toString());
    return func(params)
      .subscribe(res => {
        result.next(res.objects);
        skip = skip + res.objects.length;
        hasMore = skip < res.total;
        if (!hasMore) {
          result.complete();
        }
      });
  };

  return {result, loadMore};
}
