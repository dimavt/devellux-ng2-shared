import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { FormGroup } from '@angular/forms';

export function formErrorHandler(form: any, errors: any) {
  if (errors && errors.error_map) {
    Object.keys(errors.error_map).forEach((key => {
      if (!form.contains(key)) {
        return;
      }

      let e = {};
      e[errors.error_map[key].code] = errors.error_map[key].description;
      form.controls[key].setErrors(e);
    }));
  }
}

export function copyObject<T>(object: T): T {
  var objectCopy = <T>{};
  for (var key in object) {
    if (object.hasOwnProperty(key)) {
      objectCopy[key] = object[key];
    }
  }
  return objectCopy;
}

export function commonResponseHandler<T>(response: Response): T {
  // todo check status
  let res: T = response.json();
  return res;
}

export function commonResponseListHandler<T>(response: Response): T[] {
  let res: T[] = response.json().objects;
  return res;
}

export function commonErrorHandler(error: any): Observable<any> {
  // In a real world app, we might send the error to remote logging infrastructure
  let e;
  try {
    console.log('Request error', error.json());
    e = Observable.throw(error.json());
  } catch (error) {
    e = Observable.throw({'description': 'Server error'});
  }

  return <any> e;
}

export interface ListResponse<T> {
  objects: T[];
  total: number;
}
