import {Component, OnInit, Input, SkipSelf, Host} from "@angular/core";
import {FormControl, ControlContainer} from "@angular/forms";

/**
 * <form-errors [control]="form.controls['email']" [errors]="{required: 'Email is required'}"></form-errors>
 * <form-errors control="email" [errors]="{required: 'Email is required'}"></form-errors>
 */
@Component({
  moduleId: module.id,
  selector: 'form-errors',
  template: `
    <div *ngIf="!control.pristine && !control.valid && control.touched">
      <template ngFor let-key [ngForOf]="control.errors | keys">
          <div class="auth-cmp__input-error" role="alert">
              {{ERRORS[key]}}
          </div>
      </template>
    </div>
  `,
})
export class FormErrorsComponent implements OnInit {
  @Input() control; // FormControl|string
  @Input() errors:any = {};

  ERRORS = {
    'AUTH.EMAIL_ALREADY_TAKEN': 'Email already taken',
    'AUTH.EMAIL_NOT_FOUND': 'Email is not registered',
    'AUTH.WRONG_PASSWORD': 'Wrong password',
    isEmail: 'Invalid email address',
    required: 'Required',
    minlength: 'Password is too short',
    isMatch: 'Passwords do not match',
    pattern: 'Not allowed symbols',
  };

  constructor(@Host() @SkipSelf() private parent:ControlContainer) {
  }

  ngOnInit() {
    if (typeof this.errors === "object") {
      Object.assign(this.ERRORS, this.errors);
    }
    if (!(this.control instanceof FormControl)) {
      this.control = this.parent['form'].controls[this.control];
    }
  }
}
