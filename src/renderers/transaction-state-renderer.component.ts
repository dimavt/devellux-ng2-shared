import { Component, Input } from '@angular/core';
@Component({
  selector: 'transaction-state-renderer',
  template: `<span style="text-transform: none" [style.color]="states[state].color">{{states[state].text}}</span>`
})
export class TransactionStateRendererComponent {

  states = {
    new: {
      color: '#bd10e0', text: 'New'
    },
    closed: {
      color: '#54657e', text: 'Closed'
    },
    finished: {
      color: '#00a259', text: 'Ok',
    },
    success: {
      color: '#00a259', text: 'Ok',
    },
    pending: {
      color: '#ff9948', text: 'Pending'
    }
  }

  constructor() {

  }

  @Input() state = '';
}
