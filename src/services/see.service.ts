import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { User } from './user.service';


@Injectable()
export class SSEService {
  private source;
  private observables: {[key: string]: Observable<any>};

  constructor(private user: User) {
    this.createSource();
    // переподключаемся при изменении статуса пользователя
    this.user.status$.subscribe(() => this.createSource());
  }

  connect(eventName: string) {
    if (!Object.prototype.hasOwnProperty.call(this.observables, eventName)) {
      this.observables[eventName] = fromEvent(this.source, eventName)
        .map((e: MessageEvent) => JSON.parse(e.data))
        .catch((er) => {
          console.error(er);
          return of({});
        })
        // .do(res => console.debug(`sse ${eventName}`, res))
      ;
    }

    return this.observables[eventName];
  }

  private createSource() {
    if (this.source) {
      this.source.close();
    }
    this.clearObservables();
    if (this.user && this.user.isAuthenticated()) {
      this.source = new EventSource(`http://backend.office:8999/?auth_token=${this.user.token}`);
    } else {
      this.source = new EventSource(`http://backend.office:8999/`); // Токен - опционально
    }
    // todo написать прокси чтоб можно было передать куки на sse сервер
    // this.source = new EventSource(`http://192.168.1.13:8000`, {withCredentials: true});

  }

  private clearObservables() {
    this.observables = {};
  }

}



// Type definitions for Server-Sent Events
// Specification: http://dev.w3.org/html5/eventsource/
// Definitions by: Yannik Hampe <https://github.com/yankee42>

declare var EventSource: sse.IEventSourceStatic;

declare namespace sse {

  enum ReadyState {CONNECTING = 0, OPEN = 1, CLOSED = 2}

  interface IEventSourceStatic extends EventTarget {
    new (url: string, eventSourceInitDict?: IEventSourceInit): IEventSourceStatic;
    url: string;
    withCredentials: boolean;
    CONNECTING: ReadyState; // constant, always 0
    OPEN: ReadyState; // constant, always 1
    CLOSED: ReadyState; // constant, always 2
    readyState: ReadyState;
    onopen: Function;
    onmessage: (event: IOnMessageEvent) => void;
    onerror: Function;
    close: () => void;
  }

  interface IEventSourceInit {
    withCredentials?: boolean;
  }

  interface IOnMessageEvent {
    data: string;
  }
}
