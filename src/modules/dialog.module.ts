import {NgModule, Injectable} from "@angular/core";
import {Modal, OverlayConfig, ModalModule} from "angular2-modal/index";
import {BSModalContextBuilder, BSModalContext} from "angular2-modal/plugins/bootstrap/modal-context";
import {BootstrapModalModule} from "angular2-modal/plugins/bootstrap/bootstrap.module";

@Injectable()
export class DialogService {
  constructor(private modal:Modal) {
  }

  open<CMP,DATA>(component:{ new(...args:any[]):CMP&DATA}, data:DATA):Promise<any> {
    const builder = new BSModalContextBuilder<BSModalContext>(
      data as any,
      undefined,
      BSModalContext
    );
    let overlayConfig:OverlayConfig = {
      context: builder.toJSON()
    };
    return this.modal.open(component, overlayConfig).then(ref=> {
      return ref.result;
    });
  }
}


@NgModule({
  imports: [
    ModalModule,
    BootstrapModalModule
  ],
  providers: [
    DialogService,
    BootstrapModalModule.getProviders()
  ],
})
export class DialogModule {
  static forRoot() {

  }

  static forChild() {

  }
}
