import {NgModule, ModuleWithProviders} from "@angular/core";
import {CommonModule} from "@angular/common";
import {Http, HttpModule, RequestOptions} from "@angular/http";
import {TransactionStateRendererComponent} from "../renderers/transaction-state-renderer.component";
import {ToNowPipe} from "../pipes/to-now.pipe";
import {FromNowPipe} from "../pipes/from-now.pipe";
import {OrderStateRendererComponent} from "../renderers/order-state-renderer.component";
import {FacebookButtonDirective} from "../directives/social_buttons/facebook_button/facebook-button.directive";
import {GoogleButtonDirective} from "../directives/social_buttons/google_button/google-button.directive";
import {FormErrorsComponent} from "../helpers/form/form-errors.component";
import {KeysPipe} from "../pipes/keys.pipe";
import {ToMoneyPipe} from "../pipes/to-money.pipe";
import {HttpClient} from "../helpers/http/client";
import {AppRequestOptions} from "../helpers/http/request-options";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {DropdownSelectorComponent} from "../components/custom_inputs/dropdown-select/dropdown.component";


const arr = [
  TransactionStateRendererComponent,
  ToNowPipe,
  FromNowPipe,
  OrderStateRendererComponent,
  FacebookButtonDirective,
  GoogleButtonDirective,
  FormErrorsComponent,
  KeysPipe,
  ToMoneyPipe,
  DropdownSelectorComponent,
];


@NgModule({
  imports: [CommonModule, HttpModule, ReactiveFormsModule, FormsModule],
  declarations: arr,
  exports: arr,
})
export class DevelluxSharedModule {

  static forRoot(config = {apiUrl: ''}):ModuleWithProviders {
    const providers = [
      {
        provide: HttpClient,
        useFactory: (http) => {
          let httpClient = new HttpClient(http);
          httpClient.baseUrl = config.apiUrl;
          return httpClient;
        }, deps: [Http]
      },
      {
        provide: RequestOptions,
        useFactory: () => {
          let appRequestOptions = new AppRequestOptions();
          appRequestOptions.baseUrl = config.apiUrl;
          return appRequestOptions;
        }
      },
    ];


    return {
      ngModule: DevelluxSharedModule,
      providers,
    };
  }
}
